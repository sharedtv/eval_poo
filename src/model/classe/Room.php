<?php

namespace App\model\classe;

use App\model\classe\entite\Artwork;
use App\model\classe\entite\Film;

class Room
{
    private string $name;
    private bool $haveProjector;
    private array $artPieces = [];

    public function __construct($name, $haveProjector)
    {
        $this->name = $name;
        $this->haveProjector = $haveProjector;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getHaveProjector()
    {
        return $this->haveProjector;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setHaveProjector($haveProjector)
    {
        $this->haveProjector = $haveProjector;
    }

    public function addPiece($piece)
    {
        if ($piece instanceof Artwork || $piece instanceof Film) {
            if ($piece instanceof Film && !$this->haveProjector) {
                throw new \Exception("La salle n'a pas de projecteur");
            } else {
                $this->artPieces[] = $piece;
            }
        } else {
            throw new \Exception("La pièce n'est pas une oeuvre d'art ou un film");
        }
    }

    public function deletePiece($piece)
    {
        if ($piece instanceof Artwork || $piece instanceof Film) {
            $key = array_search($piece, $this->artPieces);
            unset($this->artPieces[$key]);
        } else {
            throw new \Exception("La pièce n'est pas une oeuvre d'art ou un film");
        }
    }
}
