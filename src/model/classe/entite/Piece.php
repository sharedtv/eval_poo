<?php

namespace App\model\classe\entite;

class Piece
{
    private string $title;
    private string $author;
    private string $owner;
    private string $domain;
    private string $code;
    private bool $exposed;
    private bool $stored;

    public function __construct($title, $author, $owner, $domain, $code, $exposed, $stored)
    {
        $this->title = $title;
        $this->author = $author;
        $this->owner = $owner;
        $this->domain = $domain;
        $this->code = $code;
        $this->exposed = $exposed;
        $this->stored = $stored;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getExposed()
    {
        return $this->exposed;
    }

    public function getStored()
    {
        return $this->stored;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }

    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function setExposed($exposed)
    {
        $this->exposed = $exposed;
    }

    public function setStored($stored)
    {
        $this->stored = $stored;
    }
}
