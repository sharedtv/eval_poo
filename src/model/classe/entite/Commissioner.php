<?php

namespace App\model\classe\entite;

class Commissioner extends Person
{
    private string $phoneNumber;

    public function __construct($firstname, $lastname, $email, $phoneNumber)
    {
        parent::__construct($firstname, $lastname, $email);
        $this->phoneNumber = $phoneNumber;
    }

    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }
}
