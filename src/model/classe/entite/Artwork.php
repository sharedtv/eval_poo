<?php

namespace App\model\classe\entite;

class Artwork extends Piece
{
    private int $weight;

    public function __construct($title, $author, $owner, $domain, $code, $exposed, $stored, $weight)
    {
        parent::__construct($title, $author, $owner, $domain, $code, $exposed, $stored);
        $this->weight = $weight;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }
}
