<?php

namespace App\model\classe;

use App\model\classe\entite\Commissioner;

class Exposure
{
    private string $title;
    private string $theme;
    private Commissioner $commissioner;
    private array $rooms = [];

    public function __construct($title, $theme, $commissioner)
    {
        $this->title = $title;
        $this->theme = $theme;
        $this->commissioner = $commissioner;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getTheme()
    {
        return $this->theme;
    }

    public function getCommissioner()
    {
        return $this->commissioner;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setTheme($theme)
    {
        $this->theme = $theme;
    }

    public function setCommissioner($commissioner)
    {
        $this->commissioner = $commissioner;
    }

    public function addRoom($room)
    {
        if ($room instanceof Room) {
            $this->rooms[] = $room;
        } else {
            throw new \Exception("Impossible d'ajouter cette salle à l'exposition!");
        }
    }

    public function deleteRoom($room)
    {
        if ($room instanceof Room) {
            $this->rooms = array_diff($this->rooms, [$room]);
        } else {
            throw new \Exception("Impossible de supprimer cette salle de l'exposition!");
        }
    }
}
