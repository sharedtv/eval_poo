<!DOCTYPE html>
<html lang="fr">

<head>
    <title><?php echo $title ?? null ?></title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0" />

</head>

<body>
    <nav>
        <ul class="menu">
            <?php echo $menu ?? null; ?>
        </ul>
        <hr>
    </nav>
    <main>
        <h1>Liste des usagers :</h1>
        <?php echo $content ?? null ?>



        <h1>Ajouter un usager :</h1>
        <form action="/?controller=author&action=add" method="post">
            <div>
                <label for="firstname">Prénom de l'usager' :</label>
                <input type="text" name="firstname" id="firstname" required>
            </div>
            <div>
                <label for="lastname">Nom de famille de l'usager :</label>
                <input type="text" name="lastname" id="lastname" required>
            </div>
            <div>
                <label for="email">Adresse e-mail de l'usager :</label>
                <input type="email" name="email" id="email" required>
            </div>
            <input type="submit" value="Ajouter">
        </form>
    </main>
</body>

</html>