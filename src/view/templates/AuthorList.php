<!DOCTYPE html>
<html lang="fr">

<head>
    <title><?php echo $title ?? null ?></title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0" />

</head>

<body>
    <nav>
        <ul class="menu">
            <?php echo $menu ?? null; ?>
        </ul>
        <hr>
    </nav>
    <main>
        <h1>Liste des auteurs :</h1>
        <table>
            <tr>
                <th>Prénom</th>
                <th>Nom</th>
                <th>E-mail</th>
                <th>Téléphone</th>
                <th>Modifier</th>
                <th>Supprimer</th>
            </tr>
            <tr>
                <td>
                    <?php echo $content['firstname'] ?? null ?>
                </td>
                <td>
                    <?php echo $content['lastname'] ?? null ?>
                </td>
                <td>
                    <?php echo $content['email'] ?? null ?>
                </td>
                <td>
                    <?php echo $content['phoneNumber'] ?? null ?>
                </td>
                <td>
                    <a href="/?controller=author&action=update&id=<?php echo $content['id'] ?? null ?>">Modifier</a>
                </td>
                <td>
                    <a href="/?controller=author&action=delete&id=<?php echo $content['id'] ?? null ?>">Supprimer</a>
                </td>
            </tr>
        </table>

        <h1>Ajouter un auteur :</h1>
        <form action="/?controller=author&action=add" method="post">
            <div>
                <label for="firstname">Prénom de l'auteur :</label>
                <input type="text" name="firstname" id="firstname" required>
            </div>
            <div>
                <label for="lastname">Nom de famille de l'auteur :</label>
                <input type="text" name="lastname" id="lastname" required>
            </div>
            <div>
                <label for="email">Adresse e-mail de l'auteur :</label>
                <input type="email" name="email" id="email" required>
            </div>
            <div>
                <label for="phoneNumber">Numéro de téléphone de l'auteur :</label>
                <input type="text" name="phoneNumber" id="phoneNumber" required>
            </div>
            <input type="submit" value="Ajouter">
        </form>
    </main>
</body>

</html>