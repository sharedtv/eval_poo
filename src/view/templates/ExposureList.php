<!DOCTYPE html>
<html lang="fr">

<head>
    <title><?php echo $title ?? null ?></title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0" />

</head>

<body>

    <nav>
        <ul class="menu">
            <?php echo $menu ?? null; ?>
        </ul>
        <hr>
    </nav>
    <main>
        <h1>Liste des expositions :</h1>
        <?php echo $content ?? null ?>



        <h1>Ajouter une exposition :</h1>
        <form action="/?controller=author&action=add" method="post">
            <div>
                <label for="firstname">Titre de l'exposition :</label>
                <input type="text" name="firstname" id="firstname" required>
            </div>
            <div>
                <label for="theme">Thème de l'exposition :</label>
                <input type="text" name="theme" id="theme" required>
            </div>
            <div>
                <label for="room">Ajouter une salle :</label>
                <select name="room" id="room">
                    <?php foreach ($rooms as $room) : ?>
                        <option value="<?php echo $room->getId() ?>">
                            <?php echo $room->getTitle() ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <input type="submit" value="Ajouter">
        </form>
    </main>
</body>

</html>