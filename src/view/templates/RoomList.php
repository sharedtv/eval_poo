<!DOCTYPE html>
<html lang="fr">

<head>
    <title><?php echo $title ?? null ?></title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0" />

</head>

<body>
    <nav>
        <ul class="menu">
            <?php echo $menu ?? null; ?>
        </ul>
        <hr>
    </nav>
    <main>
        <h1>Liste des salles :</h1>
        <?php echo $content ?? null ?>



        <h1>Ajouter une salle :</h1>
        <form action="/?controller=author&action=addAuthor" method="post">
            <div>
                <label for="firstname">Nom de la salle :</label>
                <input type="text" name="firstname" id="firstname" required>
            </div>
            <div>
                <label for="lastname">La salle dispose d'un projecteur :</label>
                <input type="checkbox" name="lastname" id="lastname" required>
            </div>
            <div>
                <label for="artPiece">Ajouter une oeuvre d'art</label>
                <select name="artPiece" id="artPiece">
                    <?php foreach ($artPieces as $artPiece) : ?>
                        <option value="<?php echo $artPiece->getId() ?>">
                            <?php echo $artPiece->getTitle() ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <input type="submit" value="Ajouter">
        </form>
    </main>
</body>

</html>