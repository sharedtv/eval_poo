<?php

namespace app\controller;

use App\model\classe\entite\Commissioner;
use App\model\classe\Exposure;
use App\model\classe\Room;
use App\router\{Request, Response};
use App\view\View;
use \Exception;

class RoomController
{
    protected $request;
    protected $response;
    protected $auth;
    protected $view;

    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    public function getView()
    {
        return $this->view;
    }

    public function execute($action)
    {
        if (method_exists($this, $action)) {
            $this->$action();
        } else {
            throw new Exception("Action {$action} non trouvée");
        }
    }

    public function defaultAction()
    {
        return $this->home();
    }

    public function home()
    {
        $commissioner = new Commissioner('Jean', 'Dupont', 'email.com', '0202020202');

        $room = new Room('Expo1', 'Thème1', $commissioner);
        $this->view = new View('templates/room.php');
        $this->view->setPart('title', "Rooms");
        $content['room'] = $room;
        $this->view->setPart('content', $room);
    }
}
