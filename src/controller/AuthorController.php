<?php

namespace app\controller;

use App\model\classe\entite\Author;
use App\router\{Request, Response};
use App\view\View;
use \Exception;

class AuthorController
{
    protected $request;
    protected $response;
    protected $auth;
    protected $view;

    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    public function getView()
    {
        return $this->view;
    }

    public function execute($action)
    {
        if (method_exists($this, $action)) {
            $this->$action();
        } else {
            throw new Exception("Action {$action} non trouvée");
        }
    }

    public function defaultAction()
    {
        return $this->home();
    }

    public function home()
    {
        // Init before being viewed
        $author = new Author('Jean', 'Dupont', 'email.com', '0202020202');
        $this->view = new View('templates/AuthorList.php');
        $this->view->setPart('title', "AuthorList");
        $content['author'] = $author;
        $content['firstname'] = 'Jean';
        $content['lastname'] = 'Dupont';
        $content['email'] = 'email.com';
        $content['phoneNumber'] = '0202020202';
        $this->view->setPart('content', $content);
    }

    public function add()
    {
        // Add data to DB 
    }
}
